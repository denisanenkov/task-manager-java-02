# PROJECT INFO

TASK MANAGER

# DEVELOPER INFO

NAME: Anenkov Denis

E-mail: denk.an@inbox.ru

# SOFTWARE

JDK version: 1.8

OS: MS Windows 10

# PROGRAM RUN COMMAND

```bash
java -jar .\task-manager.jar
```
